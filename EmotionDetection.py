# Created by ddukic on 17. 03. 2019.
import dlib
import cv2
import random
import numpy
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
import os
from collections import defaultdict
import statistics
import pandas as pd
import numpy as np

faceCascade = cv2.CascadeClassifier('C:/Users/dduki/Anaconda3/Lib/site-packages/cv2/data/haarcascade_frontalface_default.xml')
eyeCascade = cv2.CascadeClassifier('C:/Users/dduki/Anaconda3/Lib/site-packages/cv2/data/haarcascade_eye.xml')
emotions = ["happiness", "sadness", "surprise", "neutral", "other"]    # Reduced to 5 emotions
clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))            # CLAHE (Contrast Limited Adaptive Histogram
detector = dlib.get_frontal_face_detector()                            # Equalization) algorithm
predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")
clf = SVC(kernel='linear', probability=True, tol=1e-3, verbose=False)  # Set the classifier to support vector machine
clf2 = RandomForestClassifier(n_estimators=100)                        # with polynomial kernel
clf3 = SVC(kernel='poly', probability=True, tol=1e-3, gamma='auto')    # We call estimator instance clf(classifier)
                                                                       # tol = tolerance for the stopping criteria

dataSet = dict()
dataSet['landmarksVector'] = []

evaluations = os.listdir('.\\evaluations_simplified')

emotionsAsNumbers = defaultdict(list)   # Key is number of picture, values are lists of emotions represented with [1-7]

for evaluation in evaluations:
    with open('.\\evaluations_simplified\\' + evaluation) as f:
        emotionIndex = 1
        for emotionFromFile in f.readlines():
            emotionsAsNumbers[emotionIndex].append(int(emotionFromFile.strip()))
            emotionIndex += 1
    with open('.\\evaluations_simplified\\' + evaluation) as f:
        for emotionFromFile in f.readlines():
            emotionsAsNumbers[emotionIndex].append(int(emotionFromFile.strip()))
            emotionIndex += 1

emotionsMode = dict()

emotionIndex = 1

totalModeErrors = 0

for values in emotionsAsNumbers.values():
    try:
        emotionsMode[emotionIndex] = emotions[statistics.mode(values) - 1]
    except statistics.StatisticsError as e:
        print("Mode error: " + str(emotionIndex) + " ", e)
        totalModeErrors += 1
        emotionsMode[emotionIndex] = emotions[values[0] - 1]
    emotionIndex += 1

print("\nTotal number of faces with no most frequent emotion found: " + str(totalModeErrors))


def getFiles(emotion):                      # Define function to get .jpg files of faces in list
    faces = os.listdir('.\\faces')
    faces = sorted(faces, key=lambda x: int(x.split("-")[1].split(".")[0]))
    files = list()
    for key, value in emotionsMode.items():
        if emotion == value:
            files.append(faces[key-1])
    random.shuffle(files)                   # Randomly shuffle files and split 70%(training set)/20%(validation set)/10%(test set)
    training = files[:int(len(files)*0.7)]  # Get first 70% of files in training
    validation = files[int(len(files)*0.7):int(len(files)*0.9)]  # Get 20% of files in validation
    test = files[-int(len(files)*0.1):]                          # Make test set with 10%
    return training, validation, test


def createVectors(image, faceBox):
    shape = predictor(image, faceBox)  # Get the landmarks/parts for the face in box faceBox.
    x = list()
    y = list()
    for i in range(1, 68):
        x.append(float(shape.part(i).x))
        y.append(float(shape.part(i).y))
    xmean = numpy.mean(x)              # Find center of gravity
    ymean = numpy.mean(y)
    landmarksVector = list()
    for xcoord, ycoord in zip(x, y):
        landmarksVector.append(xcoord)
        landmarksVector.append(ycoord)
        mean = numpy.asarray((xmean, ymean))
        coordinates = numpy.asarray((xcoord, ycoord))
        dist = numpy.linalg.norm(coordinates - mean)  # Module(norm) of vector
        landmarksVector.append(dist)
        angle = np.degrees(np.math.atan2(ycoord - ymean, xcoord - xmean))
        landmarksVector.append(angle)
    dataSet['landmarksVector'] = landmarksVector


def getLandmarks(image):
    detections = detector(image, 1)
    for number, faceBox in enumerate(detections):
        createVectors(image, faceBox)
    if len(detections) < 1:
        detections = faceCascade.detectMultiScale(      # Detect the faces in the image
            image,
            scaleFactor=1.3,
            minNeighbors=3,
            minSize=(100, 100)
        )
        for (x, y, w, h) in detections:                 # For each detected face
            sub_face = image[y:y + h, x:x + w]
            eyes = eyeCascade.detectMultiScale(
                sub_face,
                scaleFactor=1.3,
                minNeighbors=5,
                minSize=(20, 20)
            )
            if len(eyes) is 0:
                continue
            faceBox = dlib.rectangle(int(x), int(y), int(w), int(h))
            createVectors(image, faceBox)
    if len(detections) < 1:
        dataSet['landmarksVector'] = 'error'


def makeSets():
    global totalUndetectedTraining
    global totalUndetectedValidation
    global totalUndetectedTest
    trainingData = list()
    trainingLabels = list()
    validationData = list()
    validationLabels = list()
    testData = list()
    testLabels = list()

    for emotion in emotions:
        print("working on %s" % emotion)
        training, validation, test = getFiles(emotion)

        # Append data to training, validation and test list, and generate labels 1-7

        for item in training:
            imgGray = cv2.imread('.\\faces\\' + item, cv2.IMREAD_GRAYSCALE)
            claheImage = clahe.apply(imgGray)
            getLandmarks(claheImage)

            if dataSet['landmarksVector'] == 'error':
                totalUndetectedTraining += 1
                # print("no face detected on " + item)
            else:
                trainingData.append(dataSet['landmarksVector'])
                trainingLabels.append(emotions.index(emotion) + 1)

        for item in validation:
            imgGray = cv2.imread('.\\faces\\' + item, cv2.IMREAD_GRAYSCALE)
            claheImage = clahe.apply(imgGray)
            getLandmarks(claheImage)

            if dataSet['landmarksVector'] == 'error':
                totalUndetectedValidation += 1
                # print("no face detected on " + item)
            else:
                validationData.append(dataSet['landmarksVector'])
                validationImages.append(item)
                validationLabels.append(emotions.index(emotion) + 1)

        for item in test:
            imgGray = cv2.imread('.\\faces\\' + item, cv2.IMREAD_GRAYSCALE)
            claheImage = clahe.apply(imgGray)
            getLandmarks(claheImage)

            if dataSet['landmarksVector'] == 'error':
                totalUndetectedTest += 1
                # print("no face detected on " + item)
            else:
                testData.append(dataSet['landmarksVector'])
                testImages.append(item)
                testLabels.append(emotions.index(emotion) + 1)

    return trainingData, trainingLabels, validationData, validationLabels, testData, testLabels


def printProbabilities(probabilities, images):
    imageNumber = 0
    for faceProbability in probabilities:
        imageName = images[imageNumber]
        correctEmotion = emotionsMode.get(int(imageName.split("-")[1].split(".")[0]))
        print("\nImage " + str(imageNumber + 1) + " : " + imageName + " -> emotion = " + correctEmotion + " :")
        emotionIndex = 0
        for emotionProbability in faceProbability:
            if emotions[emotionIndex] == "other":
                print(str(emotions[emotionIndex]) + " :\t\t" + str(emotionProbability))
            else:
                print(str(emotions[emotionIndex]) + " :\t" + str(emotionProbability))
            emotionIndex += 1
        imageNumber += 1


def printConfusionMatrix(probabilities, images):
    confusionMatrix = [[0, 0, 0, 0, 0],
                       [0, 0, 0, 0, 0],
                       [0, 0, 0, 0, 0],
                       [0, 0, 0, 0, 0],
                       [0, 0, 0, 0, 0]]

    imageNumber = 0
    for faceProbability in probabilities:
        imageName = images[imageNumber]
        correctEmotion = emotionsMode.get(int(imageName.split("-")[1].split(".")[0]))
        predictedEmotionProbability = max(faceProbability)
        predictedEmotion = np.array(faceProbability).tolist().index(predictedEmotionProbability)
        confusionMatrix[emotions.index(correctEmotion)][predictedEmotion] += 1
        imageNumber += 1

    cols = ["happiness", "sadness", "surprise", "neutral", "other"]
    rows = ["happiness", "sadness", "surprise", "neutral", "other"]
    print(pd.DataFrame(confusionMatrix, columns=cols, index=rows))


clfAccuracy = list()
clf2Accuracy = list()
clf3Accuracy = list()

bestModel = ()

for iteration in range(0, 10):
    validationImages = list()
    testImages = list()
    print("\nMaking sets %s" % iteration)         # Make sets by random sampling 70%(training data)/20%(validation data)
    totalUndetectedTraining = 0                   # /10%(test data)
    totalUndetectedValidation = 0
    totalUndetectedTest = 0
    trainingData, trainingLabels, validationData, validationLabels, testData, testLabels = makeSets()
    print("\nNumber of images with zero faces detected in training set: ", totalUndetectedTraining)
    print("\nNumber of images with zero faces detected in validation set: ", totalUndetectedValidation)
    print("\nNumber of images with zero faces detected in test set: ", totalUndetectedTest)
    trainingDataArray = numpy.array(trainingData)        # Turn the training set into a numpy array for the classifier
    trainingDataLabels = numpy.array(trainingLabels)
    print("\nTraining SVM linear %s" % iteration)        # Train SVM(support vector machine)
    print("Training Random forest classifier %s" % iteration)   # Train Random forest classifier
    print("Training SVM polynomial %s" % iteration)
    clf.fit(trainingDataArray, trainingLabels)
    clf2.fit(trainingDataArray, trainingLabels)
    clf3.fit(trainingDataArray, trainingLabels)
    print("\nGetting accuracies %s" % iteration)         # score() function gets accuracy
    validationDataArray = numpy.array(validationData)
    successPercentageLinear = clf.score(validationDataArray, validationLabels)
    successPercentageRandomForest = clf2.score(validationDataArray, validationLabels)
    successPercentagePolynomial = clf3.score(validationDataArray, validationLabels)
    print("\nLinear SVM: ", successPercentageLinear)
    print("Random forest classifier: ", successPercentageRandomForest)
    print("Polynomial SVM:", successPercentagePolynomial)

    print("\nLINEAR SVM confusion matrix:\n")
    linearProbabilities = clf.predict_proba(validationDataArray)
    printConfusionMatrix(linearProbabilities, validationImages)

    randomForestProbabilities = clf2.predict_proba(validationDataArray)
    print("\nRandom forest classifier confusion matrix:\n")
    printConfusionMatrix(randomForestProbabilities, validationImages)

    polynomialProbabilities = clf3.predict_proba(validationDataArray)
    print("\nPOLYNOMIAL SVM confusion matrix:\n")
    printConfusionMatrix(polynomialProbabilities, validationImages)

    # print("\nLINEAR SVM probabilities:")
    # linearProbabilities = clf.predict_proba(validationDataArray)
    # printProbabilities(linearProbabilities, validationImages)

    # print("\nRandom forest classifier probabilities:")
    # randomForestProbabilities = clf2.predict_proba(validationDataArray)
    # printProbabilities(randomForestProbabilities, validationImages)

    # print("\nPOLYNOMIAL SVM probabilities:")
    # polynomialProbabilities = clf3.predict_proba(validationDataArray)
    # printProbabilities(polynomialProbabilities, validationImages)

    success = [successPercentageLinear, successPercentageRandomForest, successPercentagePolynomial]
    models = [clf, clf2, clf3]
    modelsNames = ["linear SVM", "random forest algorithm", "polynomial SVM"]

    model = models[success.index(max(success))]
    modelName = modelsNames[success.index(max(success))]

    testDataArray = numpy.array(testData)

    if bestModel is ():
        bestModel = (model, max(success), testDataArray, testLabels, testImages, modelName)

    if max(success) > bestModel[1] and bestModel is not ():
        bestModel = (model, max(success), testDataArray, testLabels, testImages, modelName)

    clfAccuracy.append(successPercentageLinear)         # Store accuracy in a list for mean accuracy calculation
    clf2Accuracy.append(successPercentageRandomForest)
    clf3Accuracy.append(successPercentagePolynomial)

print("\nMean value linear svm: %s" % numpy.mean(clfAccuracy))
print("Mean value random forest classifier: %s" % numpy.mean(clf2Accuracy))
print("Mean value polynomial svm: %s" % numpy.mean(clf3Accuracy))

print("\nTesting on best model got " + str(bestModel[1]) + "% success rate on validation set using " + str(bestModel[5]))
successProbability = bestModel[0].score(bestModel[2], bestModel[3])
print("\nTesting on test set with best model got " + str(successProbability) + "% success rate")
successProbabilities = bestModel[0].predict_proba(bestModel[2])
print("\nIndividual probabilities for best model and test set:")
printProbabilities(successProbabilities, bestModel[4])
print("\nConfusion matrix for best model and test set:\n")
printConfusionMatrix(successProbabilities, bestModel[4])





