# Created by ddukic on 17. 03. 2019.
import cv2
import os

# set video file path of input video with name and extension
video = cv2.VideoCapture('C:\\Users\\dduki\\Desktop\\FER\\3.godina\\6.semestar\\BScThesis\\emotions_class_cut.mp4')

# get number of frames per second from video
fps = int(video.get(cv2.CAP_PROP_FPS))

# path to store photographs
path = 'C:/Users/dduki/Desktop/FER/3.godina/6.semestar/BScThesis/photographs'

# number that specifies how often (in seconds) is snapshot taken
snapshotFactor = 1
index = 0
i = 0
while True:
    ret, frame = video.read()
    if not ret:
        break
    if i % (snapshotFactor * fps) == 0:
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        cv2.imwrite(os.path.join(path, 'emotion_photo' + str(index + 1) + '.jpg'), gray)
        print('creating image: emotion_photo' + str(index + 1) + '.jpg')
        index = index + 1
    i = i + 1

video.release()
