# Created by ddukic on 17. 03. 2019.
from tkinter import *
from PIL import Image, ImageTk
import tkinter.font as tkFont
import os

image_list = os.listdir('.\\faces')

image_list = sorted(image_list, key=lambda x: int(x.split("-")[1].split(".")[0]))

emotion_list = [0] * 459

picture_index: int = 0


class EmotionWindow(Frame):

    def __init__(self, master):
        Frame.__init__(self, master)
        self.master = master

        self.master.title(image_list[0])
        photo = ImageTk.PhotoImage(Image.open('.\\faces\\' + image_list[picture_index]))
        self.image_label = Label(root, image=photo)
        self.image_label.grid(row=0, columnspan=9)
        self.image_label.image = photo

        buttonFont = tkFont.Font(family='Arial', size=15, weight=tkFont.BOLD)
        self.happy_button = Button(master, text="Happy-1", command=self.happiness, font=buttonFont)
        self.happy_button.grid(row=1, column=1, sticky='nesw')
        self.sad_button = Button(master, text="Sad-2", command=self.sadness, font=buttonFont)
        self.sad_button.grid(row=1, column=2, sticky='nesw')
        self.angry_button = Button(master, text="Angry-3", command=self.anger, font=buttonFont)
        self.angry_button.grid(row=1, column=3, sticky='nesw')
        self.disgusted_button = Button(master, text="Disgusted-4", command=self.disgust, font=buttonFont)
        self.disgusted_button.grid(row=1, column=4, sticky='nesw')
        self.scared_button = Button(master, text="Scared-5", command=self.fear, font=buttonFont)
        self.scared_button.grid(row=1, column=5, sticky='nesw')
        self.surprised_button = Button(master, text="Surprised-6", command=self.surprise, font=buttonFont)
        self.surprised_button.grid(row=1, column=6, sticky='nesw')
        self.neutral_button = Button(master, text="Neutral-7", command=self.neutral, font=buttonFont)
        self.neutral_button.grid(row=1, column=7, sticky='nesw')
        self.next_button = Button(master, text="Next picture", command=self.next, font=buttonFont, bg="green")
        self.next_button.grid(row=1, column=8, sticky='nesw')
        self.previous_button = Button(master, text="Previous picture", command=self.previous, font=buttonFont,
                                      bg="green")
        self.previous_button.grid(row=1, column=0, sticky='nesw')

        self.master.bind('<Left>', self.previous)
        self.master.bind('<Right>', self.next)
        self.master.bind('1', self.happiness)
        self.master.bind('2', self.sadness)
        self.master.bind('3', self.anger)
        self.master.bind('4', self.disgust)
        self.master.bind('5', self.fear)
        self.master.bind('6', self.surprise)
        self.master.bind('7', self.neutral)

    # noinspection PyMethodMayBeStatic
    def happiness(self, event=None):
        global picture_index
        emotion_list.__setitem__(picture_index, 1)

    # noinspection PyMethodMayBeStatic
    def sadness(self, event=None):
        global picture_index
        emotion_list.__setitem__(picture_index, 2)

    # noinspection PyMethodMayBeStatic
    def anger(self, event=None):
        global picture_index
        emotion_list.__setitem__(picture_index, 3)

    # noinspection PyMethodMayBeStatic
    def disgust(self, event=None):
        global picture_index
        emotion_list.__setitem__(picture_index, 4)

    # noinspection PyMethodMayBeStatic
    def fear(self, event=None):
        global picture_index
        emotion_list.__setitem__(picture_index, 5)

    # noinspection PyMethodMayBeStatic
    def surprise(self, event=None):
        global picture_index
        emotion_list.__setitem__(picture_index, 6)

    # noinspection PyMethodMayBeStatic
    def neutral(self, event=None):
        global picture_index
        emotion_list.__setitem__(picture_index, 7)

    def next(self, event=None):
        global picture_index
        if picture_index == 458:
            with open('emotions.txt', 'w') as f:
                i = 0
                for emotion in emotion_list:
                    if i == 458:
                        f.write("%s" % emotion)
                    else:
                        f.write("%s\n" % emotion)
                    i += 1
            self.master.destroy()
            return
        self.image_label.destroy()
        picture_index = picture_index + 1
        self.master.title(image_list[picture_index])
        photo = ImageTk.PhotoImage(Image.open('.\\faces\\' + image_list[picture_index]))
        self.image_label = Label(root, image=photo)
        self.image_label.grid(row=0, columnspan=9)
        self.image_label.image = photo

    def previous(self, event=None):
        global picture_index
        if picture_index > 0:
            self.image_label.destroy()
            picture_index = picture_index - 1
            self.master.title(image_list[picture_index])
            photo = ImageTk.PhotoImage(Image.open('.\\faces\\' + image_list[picture_index]))
            self.image_label = Label(root, image=photo)
            self.image_label.grid(row=0, columnspan=9)
            self.image_label.image = photo


root = Tk()
gui = EmotionWindow(root)
root.mainloop()
