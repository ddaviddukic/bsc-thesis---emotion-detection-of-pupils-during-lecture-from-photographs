# Created by ddukic on 17. 03. 2019.
import cv2
import os

imgId = 1
face_cascade = cv2.CascadeClassifier('C:/Users/dduki/Anaconda3/Lib/site-packages/cv2/data/haarcascade_frontalface_default.xml')

numberOfFaces = 0

for i in range(1, 361):
    imgId = i
    imgName = 'emotion_photo' + str(imgId)
    imgNameExt = 'emotion_photo' + str(imgId) + '.jpg'
    path = 'C:/Users/dduki/Desktop/FER/3.godina/6.semestar/BScThesis/'
    img = cv2.imread(path + 'photographs/' + imgNameExt, cv2.IMREAD_GRAYSCALE)

    faces = face_cascade.detectMultiScale(
        img,
        scaleFactor=1.1,
        minNeighbors=3,
        minSize=(100, 100)
    )

    index = 1
    for f in faces:
        x, y, w, h = [v for v in f]
        sub_face = img[y:y + h, x:x + w]
        cv2.imwrite(os.path.join(path, 'faces/' + imgName + '_face' + str(index) + '.jpg'), sub_face)
        index += 1

    numberOfFaces += len(faces)

print("Found {0} faces!".format(numberOfFaces))

for (x, y, w, h) in faces:
    cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)

cv2.namedWindow(imgNameExt, cv2.WINDOW_NORMAL)
cv2.resizeWindow(imgNameExt, 1000, 1000)
cv2.imshow(imgNameExt, img)
cv2.waitKey(0)
