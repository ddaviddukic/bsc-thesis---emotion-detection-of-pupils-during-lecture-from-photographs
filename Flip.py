# Created by ddukic on 17. 03. 2019.
import cv2
import os

imageList = os.listdir('.\\faces')

imageList = sorted(imageList, key=lambda x: int(x.split("-")[1].split(".")[0]))

imageIndex = 460

for image in imageList:
    img = cv2.imread('.\\faces\\' + image)
    imgGray = cv2.cvtColor(img, cv2.IMREAD_GRAYSCALE)
    verticalImg = cv2.flip(imgGray, 1)
    horizontalImg = cv2.flip(imgGray, 0)
    bothImg = cv2.flip(imgGray, -1)
    print("Creating: " + image.split("-")[0] + '-' + str(imageIndex) + '.jpg')
    cv2.imwrite(os.path.join('.\\flippedFaces\\' + image.split("-")[0] + '-' + str(imageIndex) + '.jpg'), verticalImg)
    imageIndex += 1

cv2.imshow("Original", imgGray)
cv2.imshow("Horizontal flip", horizontalImg)
cv2.imshow("Vertical flip", verticalImg)
cv2.imshow("Both flip", bothImg)
cv2.waitKey(0)
