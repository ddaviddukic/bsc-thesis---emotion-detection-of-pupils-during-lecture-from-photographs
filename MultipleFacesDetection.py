# Created by ddukic on 17. 03. 2019.
import cv2

face_cascade = cv2.CascadeClassifier(
    'C:/Users/dduki/Anaconda3/Lib/site-packages/cv2/data/haarcascade_frontalface_default.xml')
eye_cascade = cv2.CascadeClassifier('C:/Users/dduki/Anaconda3/Lib/site-packages/cv2/data/haarcascade_eye.xml')

imgId = 14
imgName = 'emotion_photo' + str(imgId)
imgNameExt = 'emotion_photo' + str(imgId) + '.jpg'
path = 'C:/Users/dduki/Desktop/FER/3.godina/6.semestar/BScThesis/'
img = cv2.imread(path + 'photographs/' + imgNameExt)
imgGray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

faces = face_cascade.detectMultiScale(
    imgGray,
    scaleFactor=1.1,
    minNeighbors=3,
    minSize=(100, 100)
)

print("Found {0} faces!".format(len(faces)))

for (x, y, w, h) in faces:
    sub_face = imgGray[y:y + h, x:x + w]
    eyes = eye_cascade.detectMultiScale(
        sub_face,
        scaleFactor=1.3,
        minNeighbors=5,
        minSize=(50, 50)
    )
    if len(eyes) is 0:
        continue
    print(len(eyes))
    cv2.rectangle(imgGray, (x, y), (x + w, y + h), (0, 255, 0), 2)
    for (ex, ey, ew, eh) in eyes:
        cv2.rectangle(sub_face, (ex, ey), (ex + ew, ey + eh), (0, 255, 0), 2)

cv2.namedWindow(imgNameExt, cv2.WINDOW_NORMAL)
cv2.resizeWindow(imgNameExt, 1000, 1000)
cv2.imshow(imgNameExt, imgGray)
cv2.waitKey(0)
