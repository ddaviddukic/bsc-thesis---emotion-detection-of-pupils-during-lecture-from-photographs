# BSC Thesis
* Title: "Emotion detection of pupils during lecture from photographs" 
* Project contains Python source code and implementation of the emotion detection algorithm using shallow machine learning (SVM, RF)
* BSc Thesis defended at Faculty of Electrical Engineering and Computing, University of Zagreb, 2019
* Thesis web page: https://zir.nsk.hr/islandora/object/fer%3A5803

