# Created by ddukic on 17. 03. 2019.
import os

evaluations = os.listdir('.\\evaluations')

for evaluation in evaluations:
    with open('.\\evaluations\\' + evaluation) as f:
        simplifiedEvaluationsList = list()
        for emotionFromFile in f.readlines():
            emotionFromFile = int(emotionFromFile.strip())
            if emotionFromFile == 6:
                simplifiedEmotion = 3
            elif emotionFromFile == 7:
                simplifiedEmotion = 4
            elif emotionFromFile in (3, 4):
                simplifiedEmotion = 5
            else:
                simplifiedEmotion = emotionFromFile
            simplifiedEvaluationsList.append(simplifiedEmotion)

    with open('.\\evaluations_simplified\\' + evaluation, 'w') as f:
        index = 0
        for simplifiedEmotion in simplifiedEvaluationsList:
            if index == 458:
                f.write("%s" % simplifiedEmotion)
            else:
                f.write("%s\n" % simplifiedEmotion)
            index += 1
