# Created by ddukic on 17. 03. 2019.
import cv2
import os

imgId = 1
face_cascade = cv2.CascadeClassifier(
    'C:/Users/dduki/Anaconda3/Lib/site-packages/cv2/data/haarcascade_frontalface_default.xml')
eye_cascade = cv2.CascadeClassifier('C:/Users/dduki/Anaconda3/Lib/site-packages/cv2/data/haarcascade_eye.xml')

numberOfFaces = 0
numberOfFacesWithEyesFound = 0

for i in range(1, 361):
    imgId = i
    imgName = 'emotion_photo' + str(imgId)
    imgNameExt = 'emotion_photo' + str(imgId) + '.jpg'
    path = 'C:/Users/dduki/Desktop/FER/3.godina/6.semestar/BScThesis/'
    img = cv2.imread(path + 'photographs/' + imgNameExt)
    imgGray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    faces = face_cascade.detectMultiScale(
        imgGray,
        scaleFactor=1.1,
        minNeighbors=3,
        minSize=(100, 100)
    )

    index = 1
    for f in faces:
        x, y, w, h = [v for v in f]
        sub_face = imgGray[y:y + h, x:x + w]
        eyes = eye_cascade.detectMultiScale(
            sub_face,
            scaleFactor=1.3,
            minNeighbors=5,
            minSize=(50, 50)
        )
        if len(eyes) is 0:
            continue
        numberOfFacesWithEyesFound += 1
        cv2.imwrite(os.path.join(path, 'facesExtractionUsingEyes/' + imgName + '_face' + str(index) + "-" +
                                 str(numberOfFacesWithEyesFound) + '.jpg'), sub_face)
        index += 1

    numberOfFaces += len(faces)

print("Found {0} faces!".format(numberOfFaces))
print("Found {0} faces with eyes".format(numberOfFacesWithEyesFound))

cv2.waitKey(0)
