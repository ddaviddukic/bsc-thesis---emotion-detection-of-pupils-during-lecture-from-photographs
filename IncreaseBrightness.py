# Created by ddukic on 17. 03. 2019.
import cv2
import os
import numpy

imageList = os.listdir('.\\faces')

imageList = sorted(imageList, key=lambda x: int(x.split("-")[1].split(".")[0]))

imageIndex = 919

for image in imageList:
    img = cv2.imread('.\\faces\\' + image)
    imgGray = cv2.cvtColor(img, cv2.IMREAD_GRAYSCALE)
    alpha = 1.5
    beta = 10
    brightnessImage = cv2.addWeighted(imgGray, alpha, numpy.zeros(imgGray.shape, imgGray.dtype), 0, beta)
    print("Creating: " + image.split("-")[0] + '-' + str(imageIndex) + '.jpg')
    cv2.imwrite(os.path.join('.\\biggerBrightnessFaces\\' + image.split("-")[0] + '-' + str(imageIndex) + '.jpg'),
                brightnessImage)
    imageIndex += 1

cv2.imshow("Original", imgGray)
cv2.imshow("Increased Brightness", brightnessImage)
cv2.waitKey(0)
