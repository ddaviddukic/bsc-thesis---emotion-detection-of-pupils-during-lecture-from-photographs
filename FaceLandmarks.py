# Created by ddukic on 17. 03. 2019.
import cv2
import dlib

face_cascade = cv2.CascadeClassifier('C:/Users/dduki/Anaconda3/Lib/site-packages/cv2/data/haarcascade_frontalface_default.xml')
eye_cascade = cv2.CascadeClassifier('C:/Users/dduki/Anaconda3/Lib/site-packages/cv2/data/haarcascade_eye.xml')

imgNameExt = 'emotion_photo29_face1-92.jpg'
path = '.\\faces\\'
img = cv2.imread(path + imgNameExt)
imgGray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")  # Landmark identifier

clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
claheImage = clahe.apply(imgGray)

detections = detector(claheImage, 1)

if len(detections) < 1:
    detections = face_cascade.detectMultiScale(                             # Detect the faces in the image
        imgGray,
        scaleFactor=1.01,
        minNeighbors=3,
        minSize=(100, 100)
    )
    for (x, y, w, h) in detections:                                         # For each detected face
        sub_face = imgGray[y:y + h, x:x + w]
        eyes = eye_cascade.detectMultiScale(
            sub_face,
            scaleFactor=1.3,
            minNeighbors=5,
            minSize=(20, 20)
        )
        if len(eyes) is 0:
            continue
        d = dlib.rectangle(int(x), int(y), int(w), int(h))
        shape = predictor(imgGray, d)       # Get coordinates
        for i in range(1, 68):      # There are 68 landmark points on each face
            cv2.circle(imgGray, (shape.part(i).x, shape.part(i).y), 1, (0, 0, 0), thickness=2)
            # For each point, draw a black circle with thickness 2
else:
    for number, faceBox in enumerate(detections):
        shape = predictor(claheImage, faceBox)      # Get the landmarks/parts for the face in box faceBox.
        for i in range(1, 68):
            cv2.circle(img, (shape.part(i).x, shape.part(i).y), 1, (0, 0, 255), thickness=2)

cv2.resizeWindow(imgNameExt, 1000, 1000)
cv2.imshow(imgNameExt, img)                         # Display the frame
cv2.waitKey(0)
